import axios from "axios";
import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { ArticleContext } from "../context/ArticleContext";

function Table({ setEditArticles }) {
  const { fetchArticles, articles, loading } = useContext(ArticleContext);

  const onDelete = async (id) => {
    try {
      const response = await axios.delete(
        `https://api-project.amandemy.co.id/api/articles/${id}`
      );
      alert("Artikel Berhasil Terhapus");
      fetchArticles();
    } catch (error) {
      console.log(error);
    }
  };

  const onUpdate = (article) => {
    setEditArticles(article);
  };
  return (
    <div className="max-w-4xl mx-auto w-full my-4">
      <div className="flex justify-end my-10">
        <Link to="/create">
          <button className="px-3 py-1 bg-blue-600 text-white rounded-lg">
            Create Article
          </button>
        </Link>
      </div>
      {loading === true ? (
        <h1 className="text-center text-2xl font-bold">Loading ...</h1>
      ) : (
        <table className="border border-gray-500 w-full">
          <thead>
            <tr>
              <th className="border border-gray-500 p-2">ID</th>
              <th className="border border-gray-500 p-2">Name</th>
              <th className="border border-gray-500 p-2">Content</th>
              <th className="border border-gray-500 p-2">Image</th>
              <th className="border border-gray-500 p-2">Highlight</th>
              <th className="border border-gray-500 p-2">Action</th>
            </tr>
          </thead>
          <tbody>
            {articles.map((article) => {
              return (
                <tr>
                  <td className="border border-gray-500 p-2">{article.id}</td>
                  <td className="border border-gray-500 p-2">{article.name}</td>
                  <td className="border border-gray-500 p-2">
                    {article.content}
                  </td>
                  <td className="border border-gray-500 p-2">
                    <img src={article.image_url} alt="" className="w-64" />
                  </td>
                  <td className="border border-gray-500 p-2">
                    {article.highlight === true ? "Aktif" : "Tidak aktif"}
                  </td>

                  <td className="border border-gray-500 p-2">
                    <div className="flex gap-2">
                      <Link to={`/update/${article.id}`}>
                        <button className="px-3 py-1 bg-yellow-600 text-white rounded-lg">
                          Update
                        </button>
                      </Link>
                      <button
                        onClick={() => {
                          onDelete(article.id);
                        }}
                        className="px-3 py-1 bg-red-600 text-white rounded-lg"
                      >
                        Delete
                      </button>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      )}
    </div>
  );
}

export default Table;
