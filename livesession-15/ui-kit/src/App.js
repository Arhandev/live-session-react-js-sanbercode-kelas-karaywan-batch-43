import { Button, Input, Layout, Menu } from "antd";
import "./App.css";

const { Search } = Input;
const { Header, Content } = Layout;

function App() {
  return (
    <div>
      <Layout>
        <Header>
          <Menu
            theme="dark"
            mode="horizontal"
            items={[
              {
                key: "item-1",
                label: "menu1",
              },
              {
                key: "item-2",
                label: "menu2",
              },
              {
                key: "item-3",
                label: "menu3",
                children: [
                  {
                    key: "sub-item-1",
                    label: "sub-menu1",
                  },
                  {
                    key: "sub-item-2",
                    label: "sub-menu2",
                  },
                ],
              },
            ]}
          />
        </Header>

        <Content style={{ padding: 20 }}>
          <Button type="primary">Klik Aku</Button>
          <Button type="primary">Klik Aku</Button>
          <div className="flex">
            <h1 className="bg-blue-400">Test 1</h1>
            <h1 className="bg-red-400">Test 2</h1>
            <h1 className="bg-green-400">Test 3</h1>
          </div>
        </Content>
      </Layout>
    </div>
  );
}

export default App;
