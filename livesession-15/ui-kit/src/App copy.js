import { Input } from "antd";
import "./App.css";

const { Search } = Input;

function App() {
  return (
    <div>
      <div
        style={{
          maxWidth: 600,
          margin: "20px auto",
        }}
      >
        <Input placeholder="Contoh Input" />
        <Search size="large" placeholder="Cari Kata..." enterButton="Cari..." allowClear />
      </div>
    </div>
  );
}

export default App;
