import React, { useEffect, useState } from "react";

function test() {
  const [counter, setCounter] = useState(20);

  useEffect(() => {
    return () => {
      console.log("Program Berjalan");
    };
  }, []);

  return (
    <div>
      <DummyComponent>
        <div>
          <h1>Hello World</h1>
          <p>
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Beatae
            asperiores ad cupiditate ipsam quis, voluptatem culpa, aspernatur
            accusantium, quam fugiat molestias dolorem consequuntur quia commodi
            a illum numquam accusamus! Assumenda.
          </p>
        </div>
      </DummyComponent>
    </div>
  );
}

export default test;
