import { useContext, useEffect, useState } from "react";
import "./App.css";
import Article from "./components/Article";
import ArticleForm from "./components/ArticleForm";
import ArticleUpdateForm from "./components/ArticleUpdateForm";
import Table from "./components/Table";
import { ArticleContext, ArticleProvider } from "./context/ArticleContext";

function App() {
  const [editArticles, setEditArticles] = useState(false);
  const { fetchArticles, articles, loading } = useContext(ArticleContext);

  useEffect(() => {
    fetchArticles();
  }, []);

  return (
      <div>
        {editArticles !== false ? (
          <ArticleUpdateForm
            editArticles={editArticles}
            setEditArticles={setEditArticles}
          />
        ) : (
          <ArticleForm />
        )}

        
        
        
      </div>
  );
}

export default App;
