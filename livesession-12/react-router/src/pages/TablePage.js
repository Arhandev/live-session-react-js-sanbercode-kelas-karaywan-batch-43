import React, { useContext, useEffect, useState } from "react";
import Article from "../components/Article";
import Navbar from "../components/Navbar";
import Table from "../components/Table";
import { ArticleContext } from "../context/ArticleContext";

function TablePage() {
  const { fetchArticles, articles, loading } = useContext(ArticleContext);
  const [editArticles, setEditArticles] = useState(false);

  useEffect(() => {
    fetchArticles();
  }, []);

  return (
    <div>
      <Navbar />
      <Table setEditArticles={setEditArticles} />
    </div>
  );
}

export default TablePage;
