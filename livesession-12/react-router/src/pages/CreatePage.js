import React from "react";
import ArticleForm from "../components/ArticleForm";
import Navbar from "../components/Navbar";

function CreatePage() {
  return (
    <div>
      <Navbar />
      <ArticleForm />
    </div>
  );
}

export default CreatePage;
