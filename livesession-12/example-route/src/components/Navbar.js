import React from "react";
import { Link } from "react-router-dom";

function Navbar() {
  return (
    <header class="shadow-lg py-4 bg-white px-12 sticky top-0">
      <nav class="mx-auto max-w-7xl flex justify-between">
        <div></div>
        <ul class="flex items-center gap-6 text-cyan-500 text-xl">
          <li>
            <Link to="/">First</Link>
          </li>
          <li>
            <Link to="/second">Second</Link>
          </li>
          <li>
            <Link to="/third">Third</Link>
          </li>
        </ul>
        <div class="flex items-center gap-4"></div>
      </nav>
    </header>
  );
}

export default Navbar;
