import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import DynamicPage from "./pages/DynamicPage";
import FirstPage from "./pages/FirstPage";
import SecondPage from "./pages/SecondPage";
import ThirdPage from "./pages/ThirdPage";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<FirstPage />} />
          <Route path="/second" element={<SecondPage />} />
          <Route path="/third" element={<ThirdPage />} />
          <Route path="/custom/:test" element={<DynamicPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
