import React from "react";
import { Helmet } from "react-helmet";
import ArticleUpdateForm from "../components/ArticleUpdateForm";
import Layout from "../components/Layout";

function UpdatePage() {
  return (
    <div>
      <Helmet>
        <title>Update Page</title>
      </Helmet>
      <Layout>
        <ArticleUpdateForm />
      </Layout>
    </div>
  );
}

export default UpdatePage;
