import React, { useState, useEffect } from "react";

function App() {
  const [counter, setCounter] = useState(0);

  const onMinus = () => {
    setCounter(counter - 1);
  };

  const onPlus = () => {
    setCounter(counter + 1);
  };

  useEffect(() => {
    alert("Nilai Counter berubah");
  }, [counter]);

  return (
    <div className="flex items-center justify-center my-8 gap-6">
      <button
        onClick={onMinus}
        className="bg-red-700 text-white p-6 rounded-lg"
      >
        Minus
      </button>
      <h1 className="text-xl">{counter}</h1>
      <button
        onClick={onPlus}
        className="bg-green-700 text-white p-6 rounded-lg"
      >
        Plus
      </button>
    </div>
  );
}

export default App;
