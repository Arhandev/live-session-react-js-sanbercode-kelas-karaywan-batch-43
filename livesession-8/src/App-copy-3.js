import React, { useState } from "react";
import Text from "./components/Text";

function App() {
  const [isDisplay, setIsDisplay] = useState(false);

  const changeDisplay = () => {
    setIsDisplay(!isDisplay);
  };
  return (
    <div className="">
      <h1 className="text-4xl text-center">Conditional Rendering</h1>
      <div className="flex flex-col items-center justify-center my-8 gap-6">
        <button onClick={changeDisplay} className="bg-red-200">
          Klik aku untuk menampilkan
        </button>
        {isDisplay === true && (
          <Text serverData={"text berhasil ditampilkan"} />
        )}
      </div>
    </div>
  );
}

export default App;
