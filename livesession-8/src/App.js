import React, { useState } from "react";
import Text from "./components/Text";
import "./custom.css";

function App() {
  const [isDisplay, setIsDisplay] = useState(false);

  const changeDisplay = () => {
    setIsDisplay(!isDisplay);
  };
  return (
    <div className="flex items-center justify-center my-6">
      <button className="button">Submit</button>
      <button className="button">Submit</button>
      <button className="button">Submit</button>
      <button className="button">Submit</button>

      <Text serverData={"Ini Text Component"} />
    </div>
  );
}

export default App;
