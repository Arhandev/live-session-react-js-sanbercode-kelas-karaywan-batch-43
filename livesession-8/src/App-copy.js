import React from "react";
import Text from "./components/Text";

const headingStyle = {
  backgroundColor: "green",
  color: "red",
};

function App() {
  const serverData = "Ini data dari server";
  const popalert = () => {
    alert("Ini berasal dari props App Component");
  };
  return (
    <div>
      <h1 className="header">Halo Dunia!</h1>
      <h2 className="second-header">Nama Aku Farhan</h2>
      <Text />
      <Text serverData={serverData} />
      <Text serverData={"Nama Saya Farhan"} />
      <Button method={popalert} />

      {isDisplay === true ? (
        <h1 className="bg-green-500">Kondisi Terpenuhi</h1>
      ) : (
        <h1 className="bg-red-500">Kondisi Tidak Terpenuhi</h1>
      )}
    </div>
  );
}

export default App;
