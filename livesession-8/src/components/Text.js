import React, { useEffect } from "react";

function Text({ serverData }) {
  // useEffect(() => {
  //   alert("Text terbuka");

  //   return () => {
  //     alert("Text Tertutup");
  //   };
  // }, []);

  return (
    <div>
      <p className="text-green-500 text-2xl">{serverData}</p>
      <button className="button">Submit</button>
    </div>
  );
}

export default Text;
