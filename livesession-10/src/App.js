import axios from "axios";
import { useEffect, useState } from "react";
import "./App.css";
import Article from "./components/Article";
import ArticleForm from "./components/ArticleForm";
import ArticleUpdateForm from "./components/ArticleUpdateForm";
import Table from "./components/Table";

function App() {
  const [articles, setArticles] = useState([]);
  const [loading, setLoading] = useState(false);
  const [editArticles, setEditArticles] = useState(false);

  const fetchArticles = async () => {
    setLoading(true);
    try {
      const response = await axios.get(
        "https://api-project.amandemy.co.id/api/articles"
      );
      console.log("Data berhasil didapatkan");
      console.log(response.data.info);
      setArticles(response.data.data); // data berhasil di masukkan ke article
      setLoading(false);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchArticles();
  }, []);

  return (
    <div>
      {editArticles !== false ? (
        <ArticleUpdateForm
          fetchArticles={fetchArticles}
          editArticles={editArticles}
          setEditArticles={setEditArticles}
        />
      ) : (
        <ArticleForm fetchArticles={fetchArticles} />
      )}

      <Table
        articles={articles}
        loading={loading}
        fetchArticles={fetchArticles}
        setEditArticles={setEditArticles}
      />
      <h1 className="text-2xl font-bold text-center mt-6 mb-4">
        List Articles
      </h1>
      {loading === true ? (
        <h1 className="text-center text-4xl font-bold my-6">
          Loading Fetch Data ...
        </h1>
      ) : (
        <div className="flex flex-col justify-center gap-6 mt-4 max-w-4xl mx-auto">
          {articles.map((article, index) => {
            return <Article key={index} article={article} />;
          })}
        </div>
      )}
    </div>
  );
}

export default App;
