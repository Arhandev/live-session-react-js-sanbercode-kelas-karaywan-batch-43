import axios from "axios";
import React, { useState } from "react";

function ArticleForm({ fetchArticles }) {
  const [input, setInput] = useState({
    name: "",
    content: "",
    image_url: "",
    highlight: false,
  });

  const handleChange = (event) => {
    if (event.target.name === "name") {
      setInput({ ...input, name: event.target.value });
    } else if (event.target.name === "content") {
      setInput({ ...input, content: event.target.value });
    } else if (event.target.name === "image_url") {
      setInput({ ...input, image_url: event.target.value });
    } else if (event.target.name === "highlight") {
      setInput({ ...input, highlight: event.target.checked });
    }
  };

  const handleSubmit = async () => {
    console.log(input);
    try {
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/articles",
        {
          name: input.name,
          content: input.content,
          image_url: input.image_url,
          highlight: input.highlight,
        }
      );
      fetchArticles();
      alert("Berhasil Membuat article");
      setInput({
        name: "",
        content: "",
        image_url: "",
        highlight: false,
      });
    } catch (error) {
      console.log(error.response.data.info);
      alert(error.response.data.info);
      console.log(error);
    }
  };

  const resetForm = () => {
    setInput({
      name: "",
      content: "",
      image_url: "",
      highlight: false,
    });
  };

  return (
    <div className="my-6 border-2 border-black rounded-xl p-8 max-w-2xl mx-auto ">
      <h1 className="text-2xl font-bold text-center mb-6">
        Form Create Articles
      </h1>
      <div className="grid grid-cols-6 gap-y-6">
        <label htmlFor="" className="col-span-2">
          Name
        </label>
        <p>:</p>
        <input
          type="text"
          placeholder="Masukkan Nama"
          className="col-span-3 border border-gray-500 p-1 rounded-lg"
          name="name"
          onChange={handleChange}
          value={input.name}
        />
        <label htmlFor="" className="col-span-2">
          Konten
        </label>
        <p>:</p>
        <input
          name="content"
          type="text"
          placeholder="Masukkan Konten"
          className="col-span-3 border border-gray-500 p-1 rounded-lg"
          onChange={handleChange}
          value={input.content}
        />
        <label htmlFor="" className="col-span-2">
          Image URL
        </label>
        <p>:</p>
        <input
          type="text"
          name="image_url"
          placeholder="Masukkan Image URL"
          className="col-span-3 border border-gray-500 p-1 rounded-lg"
          onChange={handleChange}
          value={input.image_url}
        />
        <input
          checked={input.highlight}
          onChange={handleChange}
          type="checkbox"
          name="highlight"
        />
        <label htmlFor="" className="col-span-3">
          Is Highlight?
        </label>
      </div>
      <div className="flex items-center justify-center gap-6">
        <button
          onClick={resetForm}
          className="bg-white text-green-500 border border-green-500 px-6 py-2 mt-5"
        >
          Cancel
        </button>
        <button
          onClick={handleSubmit}
          className="bg-green-300 border border-green-600 px-6 py-2 mt-5"
        >
          Create
        </button>
      </div>
    </div>
  );
}

export default ArticleForm;
