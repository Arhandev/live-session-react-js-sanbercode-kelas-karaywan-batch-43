import { useContext, useEffect, useState } from "react";
import "./App.css";
import Article from "./components/Article";
import ArticleForm from "./components/ArticleForm";
import ArticleUpdateForm from "./components/ArticleUpdateForm";
import Table from "./components/Table";
import { ArticleContext, ArticleProvider } from "./context/ArticleContext";

function App() {
  const [editArticles, setEditArticles] = useState(false);
  const { fetchArticles, articles, loading } = useContext(ArticleContext);

  useEffect(() => {
    fetchArticles();
  }, []);

  return (
      <div>
        {editArticles !== false ? (
          <ArticleUpdateForm
            editArticles={editArticles}
            setEditArticles={setEditArticles}
          />
        ) : (
          <ArticleForm />
        )}

        <Table setEditArticles={setEditArticles} />
        <h1 className="text-2xl font-bold text-center mt-6 mb-4">
          List Articles
        </h1>
        {loading === true ? (
          <h1 className="text-center text-4xl font-bold my-6">
            Loading Fetch Data ...
          </h1>
        ) : (
          <div className="flex flex-col justify-center gap-6 mt-4 max-w-4xl mx-auto">
            {articles.map((article, index) => {
              return <Article key={index} article={article} />;
            })}
          </div>
        )}
      </div>
  );
}

export default App;
