import { useContext } from "react";
import "./App.css";
import FirstComponent from "./components/FirstComponent";
import { StringContext } from "./context/StringContext";

function App() {
  const { setText } = useContext(StringContext);

  const handleChangeKalimat = () => {
    // ganti text
    setText("Halo Dunia!");
  };

  return (
    <div className="m-4 p-4 bg-black text-white">
      <h1>App Component</h1>
      <button
        onClick={handleChangeKalimat}
        className="bg-blue-200 p-4 text-black"
      >
        Klik untuk ganti Kalimat
      </button>
      <FirstComponent />
    </div>
  );
}

export default App;
