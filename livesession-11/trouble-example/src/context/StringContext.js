import { createContext, useState } from "react";

export const StringContext = createContext();

export const StringProvider = ({ children }) => {
  const [text, setText] = useState("Halo, Ini state dari Context 123");
  return (
    <StringContext.Provider
      value={{
        text: text,
        setText: setText,
      }}
    >
      {children}
    </StringContext.Provider>
  );
};
