import React, { useContext } from "react";
import { StringContext } from "../context/StringContext";

function ThirdComponent() {
  const { text } = useContext(StringContext);
  return (
    <div className="m-4 p-4 bg-blue-600 text-white">
      <h1 className="text-lg">Third Component</h1>
      <p className="text-xl text-black font-bold">{text}</p>
    </div>
  );
}

export default ThirdComponent;
