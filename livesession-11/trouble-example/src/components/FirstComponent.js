import React, { useContext } from "react";
import { StringContext } from "../context/StringContext";
import SecondComponent from "./SecondComponent";

function FirstComponent() {
  const { text } = useContext(StringContext);

  return (
    <div className="m-4 p-4 bg-red-600 text-white">
      <h1 className="text-lg">First Component</h1>
      <p className="text-xl text-black font-bold">{text}</p>

      <SecondComponent />
    </div>
  );
}

export default FirstComponent;
