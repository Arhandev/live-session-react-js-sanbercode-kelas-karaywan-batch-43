import React from "react";
import ThirdComponent from "./ThirdComponent";

function SecondComponent() {
  return (
    <div className="m-4 p-4 bg-green-600 text-white">
      <h1 className="text-lg">Second Component</h1>
      <ThirdComponent/>
    </div>
  );
}

export default SecondComponent;
