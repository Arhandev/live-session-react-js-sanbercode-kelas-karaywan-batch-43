import axios from "axios";
import { useEffect, useState } from "react";
import "./App.css";
import Article from "./components/Article";

function App() {
  const [articles, setArticles] = useState([]);

  const fetchArticles = async () => {
    try {
      const response = await axios.get(
        "https://api-project.amandemy.co.id/api/articles"
      );
      console.log("Data berhasil didapatkan");
      console.log(response.data.info);
      setArticles(response.data.data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchArticles();
  }, []);

  return (
    <div>
      
      <h1
        className="text-2xl font-bold text-center mt-6 mb-4"
      >
        List Articles
      </h1>
      <table className="border border-gray-500 w-full">
        <thead>
          <tr>
            <th className="border border-gray-500 p-2">ID</th>
            <th className="border border-gray-500 p-2">Name</th>
            <th className="border border-gray-500 p-2">Content</th>
            <th className="border border-gray-500 p-2">Image</th>
            <th className="border border-gray-500 p-2">Highlight</th>
          </tr>
        </thead>
        <tbody>
          {articles.map((article) => {
            return (
              <tr>
                <td className="border border-gray-500 p-2">{article.id}</td>
                <td className="border border-gray-500 p-2">{article.name}</td>
                <td className="border border-gray-500 p-2">
                  {article.content}
                </td>
                <td className="border border-gray-500 p-2">
                  <img src={article.image_url} alt="" className="w-64" />
                </td>
                <td className="border border-gray-500 p-2">
                  {article.highlight === false ? "Tidak Aktif" : "Aktif"}
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <div className="flex flex-col justify-center gap-6 mt-4 max-w-4xl mx-auto">
        {articles.map((article) => {
          return <Article article={article} />;
        })}
      </div>
    </div>
  );
}

export default App;
