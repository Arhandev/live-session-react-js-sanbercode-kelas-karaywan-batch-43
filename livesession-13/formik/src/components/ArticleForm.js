import axios from "axios";
import { useFormik } from "formik";
import React, { useContext } from "react";
import { useNavigate } from "react-router-dom";
import * as Yup from "yup";
import { ArticleContext } from "../context/ArticleContext";

const initialState = {
  name: "",
  content: "",
  image_url: "",
  highlight: false,
};

const schemaValidation = Yup.object({
  name: Yup.string().required("Nama Wajib diisi"),
  content: Yup.string().required("Konten Wajib diisi"),
  image_url: Yup.string()
    .required("Image Url Wajib diisi")
    .max(10, "Kata terlalu panjang")
    .url("Image Url Tidak Valid"),
  highlight: Yup.boolean(),
});

function ArticleForm() {
  const { fetchArticles } = useContext(ArticleContext);
  const navigate = useNavigate();

  const onSubmit = async () => {
    try {
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/articles",
        {
          name: values.name,
          content: values.content,
          image_url: values.image_url,
          highlight: values.highlight,
        }
      );
      fetchArticles();
      alert("Berhasil Membuat article");
      // pindah tempat
      navigate("/table");
    } catch (error) {
      console.log(error.response.data.info);
      alert(error.response.data.info);
      console.log(error);
    }
  };

  const {
    handleChange,
    values,
    handleSubmit,
    resetForm,
    errors,
    handleBlur,
    touched,
    setFieldValue,
  } = useFormik({
    initialValues: initialState,
    onSubmit: onSubmit,
    validationSchema: schemaValidation,
  });

  return (
    <div className="my-6 border-2 border-black rounded-xl p-8 max-w-2xl mx-auto ">
      <h1 className="text-2xl font-bold text-center mb-6">
        Form Create Articles
      </h1>
      <div className="grid grid-cols-6 gap-y-3">
        {/* Name Input */}
        <label htmlFor="" className="col-span-2">
          Name
        </label>
        <p>:</p>
        <input
          type="text"
          placeholder="Masukkan Nama"
          className="col-span-3 border border-gray-500 p-1 rounded-lg"
          name="name"
          onBlur={handleBlur}
          onChange={(event) => {
            if (event.target.value.length < 10) {
              setFieldValue("name", event.target.value);
              setFieldValue("content", event.target.value);
            }
          }}
          value={values.name}
        />
        <div className="col-span-3"></div>
        <p className="col-span-3 text-red-500">
          {touched.name === true && errors.name}
        </p>

        {/* Content Input */}
        <label htmlFor="" className="col-span-2">
          Konten
        </label>
        <p>:</p>
        <input
          name="content"
          onBlur={handleBlur}
          onChange={handleChange}
          value={values.content}
          type="text"
          placeholder="Masukkan Konten"
          className="col-span-3 border border-gray-500 p-1 rounded-lg"
        />
        <div className="col-span-3"></div>
        <p className="col-span-3 text-red-500">
          {touched.content === true && errors.content}
        </p>

        {/* Image URL Input */}
        <label htmlFor="" className="col-span-2">
          Image URL
        </label>
        <p>:</p>
        <input
          type="text"
          name="image_url"
          onBlur={handleBlur}
          onChange={handleChange}
          value={values.image_url}
          placeholder="Masukkan Image URL"
          className="col-span-3 border border-gray-500 p-1 rounded-lg"
        />
        <div className="col-span-3"></div>
        <p className="col-span-3 text-red-500">
          {touched.image_url === true && errors.image_url}
        </p>
        <input
          onChange={handleChange}
          onBlur={handleBlur}
          checked={values.highlight}
          type="checkbox"
          name="highlight"
        />
        <label htmlFor="" className="col-span-3">
          Is Highlight?
        </label>
      </div>
      <div className="flex items-center justify-center gap-6">
        <button
          onClick={resetForm}
          className="bg-white text-green-500 border border-green-500 px-6 py-2 mt-5"
        >
          Cancel
        </button>
        <button
          onClick={handleSubmit}
          className="bg-green-300 border border-green-600 px-6 py-2 mt-5"
        >
          Create
        </button>
      </div>
    </div>
  );
}

export default ArticleForm;
