import React, { useContext, useEffect } from "react";
import Article from "../components/Article";
import Navbar from "../components/Navbar";
import { ArticleContext } from "../context/ArticleContext";

function HomePage() {
  const { fetchArticles, articles, loading } = useContext(ArticleContext);

  useEffect(() => {
    fetchArticles();
  }, []);

  return (
    <div>
      <Navbar />
      <div className="mt-8">
        <h1 className="text-2xl font-bold text-center mt-6 mb-4">
          List Articles
        </h1>
        {loading === true ? (
          <h1 className="text-center text-4xl font-bold my-6">
            Loading Fetch Data ...
          </h1>
        ) : (
          <div className="flex flex-col justify-center gap-6 mt-4 max-w-4xl mx-auto">
            {articles.map((article, index) => {
              return <Article key={index} article={article} />;
            })}
          </div>
        )}
      </div>
    </div>
  );
}

export default HomePage;
