import React from "react";
import { useParams } from "react-router-dom";
import ArticleUpdateForm from "../components/ArticleUpdateForm";
import Navbar from "../components/Navbar";

function UpdatePage() {
  
  return (
    <div>
      <Navbar />
      <ArticleUpdateForm />
    </div>
  );
}

export default UpdatePage;
