import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { ArticleContext } from "../context/ArticleContext";

import { useFormik } from "formik";
import * as Yup from "yup";

const initialState = {
  name: "",
  content: "",
  image_url: "",
  highlight: false,
};

const schemaValidation = Yup.object({
  name: Yup.string().required("Nama Wajib diisi"),
  content: Yup.string().required("Konten Wajib diisi"),
  image_url: Yup.string()
    .required("Image Url Wajib diisi")
    .url("Image Url Tidak Valid"),
  highlight: Yup.boolean(),
});

function ArticleUpdateForm() {
  const { fetchArticles } = useContext(ArticleContext);
  const { id } = useParams();
  const navigate = useNavigate();
  const [input, setInput] = useState(initialState);

  const handleUpdate = async () => {
    try {
      const response = await axios.put(
        `https://api-project.amandemy.co.id/api/articles/${id}`,
        {
          name: values.name,
          content: values.content,
          image_url: values.image_url,
          highlight: values.highlight,
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      fetchArticles();
      alert("Berhasil Mengupdate article");
      navigate("/table");
    } catch (error) {
      alert(error.response.data.info);
      console.log(error);
    }
  };

  const fetchArticleDetail = async () => {
    try {
      const response = await axios.get(
        `https://api-project.amandemy.co.id/api/articles/${id}`
      );
      console.log(response.data.data);
      setInput({
        name: response.data.data.name,
        content: response.data.data.content,
        image_url: response.data.data.image_url,
        highlight: response.data.data.highlight,
      });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    // fetch article detail
    fetchArticleDetail();
  }, []);

  const {
    handleChange,
    values,
    handleSubmit,
    resetForm,
    errors,
    handleBlur,
    touched,
    setFieldValue,
  } = useFormik({
    initialValues: input,
    onSubmit: handleUpdate,
    validationSchema: schemaValidation,
    enableReinitialize: true,
  });

  return (
    <div className="my-6 border-2 border-solid border-black rounded-xl p-8 max-w-2xl mx-auto ">
      <h1 className="text-2xl font-bold text-center mb-6">
        Form Update Articles
      </h1>
      <div className="grid grid-cols-6 gap-y-6">
        {/* Name Input */}

        <label htmlFor="" className="col-span-2">
          Name
        </label>
        <p>:</p>
        <input
          type="text"
          placeholder="Masukkan Nama"
          className="col-span-3 border border-gray-500 p-1 rounded-lg"
          name="name"
          onChange={handleChange}
          value={values.name}
          onBlur={handleBlur}
        />
        <div className="col-span-3"></div>
        <p className="col-span-3 text-red-500">
          {touched.name === true && errors.name}
        </p>

        {/* Content Input */}
        <label htmlFor="" className="col-span-2">
          Konten
        </label>
        <p>:</p>
        <input
          name="content"
          type="text"
          placeholder="Masukkan Konten"
          className="col-span-3 border border-gray-500 p-1 rounded-lg"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.content}
        />
        <div className="col-span-3"></div>
        <p className="col-span-3 text-red-500">
          {touched.content === true && errors.content}
        </p>

        {/* Image URL Input */}
        <label htmlFor="" className="col-span-2">
          Image URL
        </label>
        <p>:</p>
        <input
          type="text"
          name="image_url"
          placeholder="Masukkan Image URL"
          className="col-span-3 border border-gray-500 p-1 rounded-lg"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.image_url}
        />
        <div className="col-span-3"></div>
        <p className="col-span-3 text-red-500">
          {touched.image_url === true && errors.image_url}
        </p>

        <input
          checked={values.highlight}
          onChange={handleChange}
          onBlur={handleBlur}
          type="checkbox"
          name="highlight"
        />
        <label htmlFor="" className="col-span-3">
          Is Highlight?
        </label>
      </div>
      <div className="flex items-center justify-center">
        <button
          onClick={handleSubmit}
          className="bg-green-300 border border-green-600 px-6 py-2 mt-5"
        >
          Update
        </button>
      </div>
    </div>
  );
}

export default ArticleUpdateForm;
