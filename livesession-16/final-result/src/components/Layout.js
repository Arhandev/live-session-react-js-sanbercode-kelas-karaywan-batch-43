import { Button, Layout as AntdLayout, Menu } from "antd";
import axios from "axios";
import React from "react";
import { Link, useNavigate } from "react-router-dom";

const { Header, Content } = AntdLayout;

function Layout({ children }) {
  const navigate = useNavigate();

  const onLogout = async () => {
    try {
      const response = axios.post(
        "https://api-project.amandemy.co.id/api/logout",
        {},
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
    } catch (error) {
      console.log(error);
    } finally {
      localStorage.removeItem("token");
      localStorage.removeItem("user");
      alert("Berhasil Logout");
      navigate("/login");
    }
  };

  return (
    <div>
      <AntdLayout>
        <Header className="flex justify-between">
          <Menu
            theme="dark"
            mode="horizontal"
            items={[
              {
                key: "home",
                label: <Link to="/">Home</Link>,
              },
              {
                key: "table",
                label: <Link to="/table">Table</Link>,
              },
            ]}
          />

          <div>
            {localStorage.getItem("token") !== null ? (
              <div className="flex items-center gap-8">
                <p className="m-0 text-white">
                  Hai, {localStorage.getItem("user")}
                </p>
                <Button onClick={onLogout} danger type="primary">
                  Logout
                </Button>
              </div>
            ) : (
              <div className="flex items-center gap-3">
                <Link to="/login">
                  <Button type="primary">Login</Button>
                </Link>
                <Link to="/register">
                  <Button type="default">Register</Button>
                </Link>
              </div>
            )}
          </div>
        </Header>
        <Content className="px-6">
          <div className="bg-white min-h-screen">{children}</div>
        </Content>
      </AntdLayout>
    </div>
  );
}

export default Layout;
