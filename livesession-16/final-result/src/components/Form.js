import React, { useState } from "react";

function Form() {
  const [input, setInput] = useState({
    nama: "",
    harga: "",
    deskripsi: "",
    genre: "",
    is_active: false,
  });

  const handleChange = (event) => {
    if (event.target.name === "nama") {
      setInput({ ...input, nama: event.target.value });
    } else if (event.target.name === "harga") {
      setInput({ ...input, harga: event.target.value });
    } else if (event.target.name === "deskripsi") {
      setInput({ ...input, deskripsi: event.target.value });
    } else if (event.target.name === "genre") {
      setInput({ ...input, genre: event.target.value }); // select
    } else if (event.target.name === "is_active") {
      setInput({ ...input, is_active: event.target.checked }); //checkbox
    }
  };

  const handleSubmit = () => {
    console.log(input);
  };

  return (
    <div>
      <section className="max-w-xl mx-auto border-2 border-solid border-gray-600 p-6 mt-12">
        <h1 className="text-center">Form</h1>
        <div className="my-4">
          <label htmlFor="">Nama:</label>
          <input
            name="nama"
            type="text"
            placeholder="Masukkan nama"
            className="ml-10 border-2 border-gray-600 rounded-md"
            onChange={handleChange}
          />
        </div>
        <div className="my-4">
          <label htmlFor="">Harga:</label>
          <input
            type="text"
            placeholder="Masukkan harga"
            className="ml-10 border-2 border-gray-600 rounded-md"
            onChange={handleChange}
            name="harga"
          />
        </div>
        <div className="my-4">
          <label htmlFor="">Deskripsi:</label>
          <input
            type="text"
            placeholder="Masukkan deskripsi"
            className="ml-4 border-2 border-gray-600 rounded-md"
            onChange={handleChange}
            name="deskripsi"
          />
        </div>
        <div className="my-4">
          <label htmlFor="">Is Active:</label>
          <input
            type="checkbox"
            className="ml-4 border-2 border-gray-600 rounded-md"
            onChange={handleChange}
            name="is_active"
          />
        </div>
        <div className="my-4">
          <label htmlFor="">Genre:</label>
          <select onChange={handleChange} name="genre" id="">
            <option value="abc">ABC</option>
            <option value="cdf">CDF</option>
            <option value="ghi">GHI</option>
          </select>
        </div>

        <button onClick={handleSubmit} className="btn-submit">
          Klik aku
        </button>
      </section>
    </div>
  );
}

export default Form;
