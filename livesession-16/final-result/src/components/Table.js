import { Button, ConfigProvider, Table as TableAntd } from "antd";
import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import { ArticleContext } from "../context/ArticleContext";

const MySwal = withReactContent(Swal);

function Table() {
  const columns = [
    {
      key: "ID",
      dataIndex: "id",
      title: "ID",
    },
    {
      key: "name",
      dataIndex: "name",
      title: "Name",
    },
    {
      key: "content",
      dataIndex: "content",
      title: "Content",
    },
    {
      key: "image_url",
      dataIndex: "image_url",
      title: "Image",
      render: (_, record) => {
        return <img src={record.image_url} className="w-32" alt="image" />;
      },
    },
    {
      key: "highlight",
      dataIndex: "highlight",
      title: "Highlight",
      render: (_, record) => {
        if (record.highlight === true) {
          return <p>Aktif</p>;
        } else {
          return <p>Tidak Aktif</p>;
        }
      },
    },
    {
      key: "user",
      dataIndex: "user",
      title: "Created By",
      render: (_, record) => {
        if (record.user === null) {
          return <p>-</p>;
        } else {
          return <p>{record.user.username}</p>;
        }
      },
    },
    {
      key: "action",
      dataIndex: "action",
      title: "Action",
      render: (_, record) => {
        return (
          <div className="flex gap-2">
            <Link to={`/update/${record.id}`}>
              <ConfigProvider
                theme={{
                  token: {
                    colorPrimary: "#d8ad00",
                  },
                }}
              >
                <Button type="primary">Update</Button>
              </ConfigProvider>
            </Link>
            <Button
              onClick={() => {
                onDelete(record.id);
              }}
              danger
              type="primary"
            >
              Delete
            </Button>
          </div>
        );
      },
    },
  ];

  const { fetchArticles, articles, loading } = useContext(ArticleContext);
  const [filter, setFilter] = useState({
    search: "",
    highlight: "",
  });
  const [articlesFilter, setArticlesFilter] = useState([]);

  const onDelete = async (id) => {
    try {
      const result = await MySwal.fire({
        title: (
          <p className="text-5xl text-green-600">
            Apakah Yakin ingin menghapus?
          </p>
        ),
        icon: "info",
        showCancelButton: true,
        confirmButtonText: "Hapus",
        cancelButtonText: "Cancel",
      });
      // let confirm = window.confirm("Test Confirm");

      if (result.isConfirmed === true) {
        const response = await axios.delete(
          `https://api-project.amandemy.co.id/api/articles/${id}`
        );
        MySwal.fire({
          title: "Artikel Berhasil Terhapus",
          icon: "success",
        });
        fetchArticles();
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleChange = (event) => {
    if (event.target.name === "search") {
      setFilter({ ...filter, search: event.target.value });
    } else if (event.target.name === "highlight") {
      setFilter({ ...filter, highlight: event.target.value });
    }
  };

  const handleSearch = () => {
    let articleArr = structuredClone(articles);
    if (filter.search !== "") {
      articleArr = articleArr.filter((article) => {
        return article.name.toLowerCase().includes(filter.search.toLowerCase());
      });
    }

    if (filter.highlight !== "") {
      articleArr = articleArr.filter((article) => {
        return article.highlight.toString() === filter.highlight;
      });
    }

    setArticlesFilter(articleArr);
  };

  const handleReset = () => {
    setFilter({
      search: "",
      highlight: "",
    });
    setArticlesFilter(articles)
  };

  useEffect(() => {
    setArticlesFilter(articles);
  }, [articles]);

  return (
    <div className="max-w-4xl mx-auto w-full my-4 py-1">
      <div className="flex justify-end my-10">
        <Link to="/create">
          <Button type="primary" size="large">
            Create Article
          </Button>
        </Link>
      </div>
      <div className="flex justify-end my-10 gap-4">
        <select
          id=""
          className="py-2 px-2 rounded-md bg-white border border-gray-400 w-48"
          name="highlight"
          onChange={handleChange}
          value={filter.highlight}
        >
          <option value="" disabled>
            Filter highlight
          </option>
          <option value="true">Aktif</option>
          <option value="false">Tidak Aktif</option>
        </select>
        <input
          type="text"
          name="search"
          className="rounded-lg px-2 py-1"
          placeholder="Search"
          onChange={handleChange}
          value={filter.search}
        />
        <Button onClick={handleSearch} type="primary" size="large">
          Search
        </Button>
        <Button onClick={handleReset} danger type="primary" size="large">
          Reset
        </Button>
      </div>
      <TableAntd
        loading={loading}
        columns={columns}
        dataSource={articlesFilter}
      />
    </div>
  );
}

export default Table;
