import React, { useContext, useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import Layout from "../components/Layout";
import Table from "../components/Table";
import { ArticleContext } from "../context/ArticleContext";

function TablePage() {
  const { fetchArticles, articles, loading } = useContext(ArticleContext);

  useEffect(() => {
    fetchArticles();
  }, []);

  return (
    <div>
      <Helmet>
        <title>Table Page</title>
      </Helmet>
      <Layout>
        
        <Table/>
      </Layout>
    </div>
  );
}

export default TablePage;
