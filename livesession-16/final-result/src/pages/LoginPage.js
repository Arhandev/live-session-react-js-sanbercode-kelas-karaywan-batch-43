import axios from "axios";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import Layout from "../components/Layout";

function LoginPage() {
  const navigate = useNavigate();

  const [input, setInput] = useState({
    email: "",
    password: "",
  });

  const handleChange = (event) => {
    if (event.target.name === "email") {
      setInput({ ...input, email: event.target.value });
    } else if (event.target.name === "password") {
      setInput({ ...input, password: event.target.value });
    }
  };

  const onSubmit = async () => {
    try {
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/login",
        {
          email: input.email,
          password: input.password,
        }
      );
      localStorage.setItem("token", response.data.data.token);
      localStorage.setItem("user", response.data.data.user.username);
      alert("Berhasil Login");
      navigate("/table");
    } catch (error) {
      alert(error.response.data.info);
    }
  };

  return (
    <div>
      <Layout>
        <div className="my-6 py-4">
          <main className="border-2 border-solid rounded-lg shadow-lg max-w-lg mx-auto p-6">
            <h1 className="text-center text-2xl my-3">Form Login</h1>
            <div className="flex flex-col gap-3">
              <div>
                <label htmlFor="">Email:</label>
                <input
                  name="email"
                  onChange={handleChange}
                  type="text"
                  className="w-full border border-gray-300 rounded"
                />
              </div>
              <div>
                <label htmlFor="">Password:</label>
                <input
                  name="password"
                  onChange={handleChange}
                  type="password"
                  className="w-full border border-gray-300 rounded"
                />
              </div>
              <div className="flex items-center justify-center mt-2">
                <button
                  onClick={onSubmit}
                  className="bg-blue-600 text-white py-2 px-10 rounded-lg"
                >
                  Login
                </button>
              </div>
            </div>
          </main>
        </div>
      </Layout>
    </div>
  );
}

export default LoginPage;
