import axios from "axios";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import Layout from "../components/Layout";

function RegisterPage() {
  const navigate = useNavigate();
  const [input, setInput] = useState({
    name: "",
    username: "",
    email: "",
    password: "",
    confirmation_password: "",
  });

  const handleChange = (event) => {
    if (event.target.name === "name") {
      setInput({ ...input, name: event.target.value });
    } else if (event.target.name === "email") {
      setInput({ ...input, email: event.target.value });
    } else if (event.target.name === "username") {
      setInput({ ...input, username: event.target.value });
    } else if (event.target.name === "password") {
      setInput({ ...input, password: event.target.value });
    } else if (event.target.name === "password_confirmation") {
      setInput({ ...input, password_confirmation: event.target.value });
    }
  };

  const onSubmit = async () => {
    try {
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/register",
        {
          name: input.name,
          username: input.username,
          email: input.email,
          password: input.password,
          password_confirmation: input.password_confirmation,
        }
      );
      alert("Berhasil Register");
      navigate("/login");
    } catch (error) {
      alert(error.response.data.info);
    }
  };

  return (
    <div>
      <Layout>
        <div className="my-6 py-4">
          <main className="border-2 border-solid rounded-lg shadow-lg max-w-lg mx-auto p-6">
            <h1 className="text-center text-2xl my-3">Form Register</h1>
            <div className="flex flex-col gap-3">
              <div>
                <label htmlFor="">Nama:</label>
                <input
                  name="name"
                  onChange={handleChange}
                  type="text"
                  className="w-full border border-solid border-gray-300 rounded"
                />
              </div>
              <div>
                <label htmlFor="">Email:</label>
                <input
                  name="email"
                  onChange={handleChange}
                  type="text"
                  className="w-full border border-solid border-gray-300 rounded"
                />
              </div>
              <div>
                <label htmlFor="">Username:</label>
                <input
                  name="username"
                  onChange={handleChange}
                  type="text"
                  className="w-full border border-solid border-gray-300 rounded"
                />
              </div>
              <div>
                <label htmlFor="">Password:</label>
                <input
                  name="password"
                  onChange={handleChange}
                  type="password"
                  className="w-full border border-solid border-gray-300 rounded"
                />
              </div>
              <div>
                <label htmlFor="">Konfirmasi Password:</label>
                <input
                  name="password_confirmation"
                  onChange={handleChange}
                  type="password"
                  className="w-full border border-solid border-gray-300 rounded"
                />
              </div>
              <div className="flex items-center justify-center mt-2">
                <button
                  onClick={onSubmit}
                  className="bg-blue-600 text-white py-2 px-10 rounded-lg"
                >
                  Register
                </button>
              </div>
            </div>
          </main>
        </div>
      </Layout>
    </div>
  );
}

export default RegisterPage;
