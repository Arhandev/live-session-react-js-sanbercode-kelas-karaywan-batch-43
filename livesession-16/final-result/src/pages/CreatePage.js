import React from "react";
import { Helmet } from "react-helmet";
import ArticleForm from "../components/ArticleForm";
import Layout from "../components/Layout";

function CreatePage() {
  return (
    <div>
      <Helmet>
        <title>Create Product</title>
      </Helmet>
      <Layout>
        <ArticleForm />
      </Layout>
    </div>
  );
}

export default CreatePage;
