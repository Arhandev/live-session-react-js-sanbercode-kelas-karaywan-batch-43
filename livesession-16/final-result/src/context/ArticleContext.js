import axios from "axios";
import { createContext, useState } from "react";

export const ArticleContext = createContext();

export const ArticleProvider = ({ children }) => {
  const [articles, setArticles] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchArticles = async () => {
    setLoading(true);
    try {
      const response = await axios.get(
        "https://api-project.amandemy.co.id/api/articles",
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      setArticles(response.data.data); // data berhasil di masukkan ke article
      setLoading(false);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <ArticleContext.Provider
      value={{
        articles: articles,
        setArticles: setArticles,
        loading: loading,
        setLoading: setLoading,
        fetchArticles: fetchArticles,
      }}
    >
      {children}
    </ArticleContext.Provider>
  );
};
