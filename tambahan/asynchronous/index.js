const axios = require("axios");

let loading = false;

async function fetchData() {
  try {
    loading = true;
    const response = await axios.get(
      "https://api-project.amandemy.co.id/api/articles"
    );
    console.log(response.data);
  } catch (error) {
    console.log(error);
  } finally {
    loading = false;
  }
}

function kuadrat(angka) {
  return angka * angka;
}

const kuadratArrow = (angka) => angka * angka;

let result = kuadratArrow(12);

console.log(result);
